package main

import (
	"context"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

type Config struct {
	Credentials           string
	Customer              string
	ImpersonatedUserEmail string
}

func Provider() *schema.Provider {
	p := &schema.Provider{
		Schema: map[string]*schema.Schema{
			"credentials": {
				Description: "Either the path to or the contents of a service account key file in JSON format " +
					"you can manage key files using the Cloud Console).  If not provided, the application default " +
					"credentials will be used.",
				Type:     schema.TypeString,
				Optional: true,
				DefaultFunc: schema.MultiEnvDefaultFunc([]string{
					"GOOGLEWORKSPACE_CREDENTIALS",
					"GOOGLEWORKSPACE_CLOUD_KEYFILE_JSON",
					"GOOGLE_CREDENTIALS",
				}, nil),
			},

			"customer_id": {
				Description: "The customer id provided with your Google Workspace subscription. It is found " +
					"in the admin console under Account Settings.",
				Type: schema.TypeString,
				DefaultFunc: schema.MultiEnvDefaultFunc([]string{
					"GOOGLEWORKSPACE_CUSTOMER_ID",
				}, nil),
				Optional: true,
			},

			"impersonated_user_email": {
				Description: "The impersonated user's email with access to the Admin APIs can access the Admin SDK Directory API. " +
					"`impersonated_user_email` is required for all services except group and user management.",
				Type: schema.TypeString,
				DefaultFunc: schema.MultiEnvDefaultFunc([]string{
					"GOOGLEWORKSPACE_IMPERSONATED_USER_EMAIL",
				}, nil),
				Optional: true,
			},
		},

		DataSourcesMap: map[string]*schema.Resource{
			"rahasak_licensing": dataSourceLicensing(),
		},

		ResourcesMap: map[string]*schema.Resource{
			"rahasak_uuid": resourceUuid(),
		},
	}

	p.ConfigureContextFunc = configure(p)
	return p
}

func configure(p *schema.Provider) func(context.Context, *schema.ResourceData) (interface{}, diag.Diagnostics) {
	return func(ctx context.Context, d *schema.ResourceData) (interface{}, diag.Diagnostics) {
		var diags diag.Diagnostics
		config := Config{}

		// Get credentials
		if v, ok := d.GetOk("credentials"); ok {
			config.Credentials = v.(string)
		}

		// Get customer id
		if v, ok := d.GetOk("customer_id"); ok {
			config.Customer = v.(string)
		} else {
			diags = append(diags, diag.Diagnostic{
				Severity: diag.Error,
				Summary:  "customer_id is required",
			})

			return nil, diags
		}

		// Get impersonated user email
		if v, ok := d.GetOk("impersonated_user_email"); ok {
			config.ImpersonatedUserEmail = v.(string)
		}

		return &config, diags
	}
}
