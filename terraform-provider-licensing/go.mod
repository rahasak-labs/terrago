module terraform-provider-licensing

go 1.16

require (
	github.com/hashicorp/terraform-plugin-sdk/v2 v2.10.1
	golang.org/x/oauth2 v0.0.0-20220411215720-9780585627b5
	google.golang.org/api v0.79.0
)
