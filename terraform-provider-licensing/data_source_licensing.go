package main

import (
	"context"

	googleoauth "golang.org/x/oauth2/google"
	licensing "google.golang.org/api/licensing/v1"
	"google.golang.org/api/option"
	"google.golang.org/api/transport"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

func dataSourceLicensing() *schema.Resource {
	return &schema.Resource{
		Description: "Licensing data source in the Terraform Googleworkspace provider. Licensing resides " +
			"under the `https://www.googleapis.com/auth/apps.licensing` client scope.",

		ReadContext: dataSourceLicensesRead,

		Schema: map[string]*schema.Schema{
			"assignments": &schema.Schema{
				Type:     schema.TypeList,
				Computed: true,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"product_id": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"user_id": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"sku_id": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"sku_name": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"product_name": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
					},
				},
			},
			"product_id": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Default:  "Google-Apps",
			},
			"sku_id": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Default:  "",
			},
			"max_results": &schema.Schema{
				Type:     schema.TypeInt,
				Optional: true,
				Default:  100,
			},
		},
	}
}

func dataSourceLicensesRead(ctx context.Context, d *schema.ResourceData, meta interface{}) diag.Diagnostics {
	config := meta.(*Config)

	//credentials := d.Get("credentials").(string)
	//customer_id := d.Get("customer_id").(string)
	//impersonated_user_email = d.Get("impersonated_user_email").(string)

	credParams := googleoauth.CredentialsParams{
		Scopes:  []string{"https://www.googleapis.com/auth/apps.licensing"},
		Subject: config.ImpersonatedUserEmail,
	}
	creds, err := googleoauth.CredentialsFromJSONWithParams(ctx, []byte(config.Credentials), credParams)
	if err != nil {
		return diag.FromErr(err)
	}

	client, _, err := transport.NewHTTPClient(ctx, option.WithTokenSource(creds.TokenSource))
	if err != nil {
		return diag.FromErr(err)
	}

	service, err := licensing.NewService(ctx, option.WithHTTPClient(client))
	if err != nil {
		return diag.FromErr(err)
	}

	var assignments []map[string]interface{}
	var pageToken string
	productId := d.Get("product_id").(string)
	skuId := d.Get("sku_id").(string)
	maxResults := d.Get("max_results").(int)

	for {
		resp, err := service.LicenseAssignments.ListForProductAndSku(productId, skuId, config.Customer).MaxResults(int64(maxResults)).PageToken(pageToken).Do()
		if err != nil {
			return diag.FromErr(err)
		}

		for _, assignment := range resp.Items {
			assignmentMap := map[string]interface{}{
				"user_id":      assignment.UserId,
				"product_id":   assignment.ProductId,
				"sku_id":       assignment.SkuId,
				"sku_name":     assignment.SkuName,
				"product_name": assignment.ProductName,
			}
			assignments = append(assignments, assignmentMap)
		}
		if resp.NextPageToken == "" {
			break
		}

		pageToken = resp.NextPageToken
	}

	if err := d.Set("assignments", assignments); err != nil {
		return diag.FromErr(err)
	}

	d.SetId("assignments")

	return nil
}
