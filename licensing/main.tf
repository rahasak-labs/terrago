data "rahasak_licensing" "all-licenses" {
  product_id = "Google-Apps"
}

data "rahasak_licensing" "enterprice-licenses" {
  product_id = "Google-Apps"
  sku_id = "1010010011"
}

data "rahasak_licensing" "starter-licenses" {
  product_id = "Google-Apps"
  sku_id = "1010010011"
}

output "total_licenses_count" {
  value = length(data.rahasak_licensing.all-licenses.assignments)
}

output "starter_licenses_count" {
  value = length(data.rahasak_licensing.enterprice-licenses.assignments)
}

output "enterpice_licenses_count" {
  value = length(data.rahasak_licensing.starter-licenses.assignments)
}
