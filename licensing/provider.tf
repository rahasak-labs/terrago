terraform {
  required_providers {
    rahasak = {
      source  = "terraform.rahasak.dev/rahasak/licensing"
      version = "1.0.0"
    }
  }
}

provider "rahasak" {
  customer_id             = "11111111"
  credentials             = base64decode(var.googleworkspace_sa_base64)
  impersonated_user_email = "account-admin@rahasak.com"
}
